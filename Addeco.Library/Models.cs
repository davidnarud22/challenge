﻿namespace Addeco.Library
{
    public class Models
    {
        public class PositionInArray
        {
            public int Row { get; set; }
            public int Column { get; set; }
        }

        public class Position
        {
            public int Id { get; set; }
            public int IdNextPosition { get; set; }
            public int ValueOfPosition { get; set; }
            public int PositionInRow { get; set; }
            public PositionInArray PositionInArray { get; set; }
        }

        public class Response
        {
            public int HighDrop { get; set; }
            public int LenghtOfPositions { get; set; }
            public string RouteForHighestDrop { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static Addeco.Library.Models;

namespace Addeco.Library
{
    public class CalculatedRoute
    {
        #region Declarations
        private const int Up = 1;
        private const int Down = 2;
        private const int Right = 3;
        private const int Left = 4;
        private int _newId = 1;
        private int _actualRowInArray = 0;
        private int _calculatedRouteLength;
        private int[] _routeForHighestDrop;
        private int[][] _dataMap;
        private List<int[]> _fastestRoute = new List<int[]>();
        private List<Position> _positions = new List<Position>();
        #endregion

        #region Methods
        public Response CalculatedPath(string path)
        {
            ReadTxtAndConvertDataToUse(path);
            CalculedPossibleRoutes();
            return GetResults();
        }

        private void ReadTxtAndConvertDataToUse(string path)
        {
            _dataMap = File.ReadLines(path)
                .Skip(1)
                .Select(x => x.Split(' ').Select(y => int.Parse(y))
                .ToArray())
                .ToArray();

            for (int row = 0; row < _dataMap.Length; row++)
            {
                for (int column = 0; column < _dataMap[row].Length; column++)
                {
                    PositionInArray newPositionInArray = new PositionInArray()
                    {
                        Row = row,
                        Column = column
                    };

                    Position newPosition = new Position()
                    {
                        Id = _newId++,
                        IdNextPosition = 0,
                        ValueOfPosition = _dataMap[row][column],
                        PositionInRow = 0,
                        PositionInArray = newPositionInArray
                    };

                    _positions.Add(newPosition);
                }
            }
        }

        private int CalculedPossibleRoutes()
        {
            int count = 0;
            _positions.Where(x => x.PositionInRow == _actualRowInArray)
                .ToArray()
                .ToList()
                .ForEach(position =>
                {
                    count +=
                   CalculateNextMove(Up, position) +
                    CalculateNextMove(Down, position) +
                    CalculateNextMove(Right, position) +
                    CalculateNextMove(Left, position);
                });

            _actualRowInArray++;
            if (count > 0) { CalculedPossibleRoutes(); }
            return count;
        }

        private int CalculateNextMove(int move, Position position)
        {
            PositionInArray nextPositionInArray = new PositionInArray();
            bool itsPossibleNextMove = false;

            if (move == Up)
            {
                if (position.PositionInArray.Row - 1 >= 0)
                {
                    nextPositionInArray.Row = position.PositionInArray.Row - 1;
                    nextPositionInArray.Column = position.PositionInArray.Column;
                    itsPossibleNextMove = true;
                }
            }
            else if (move == Down)
            {
                if (position.PositionInArray.Row + 1 < _dataMap.GetLength(0))
                {
                    nextPositionInArray.Row = position.PositionInArray.Row + 1;
                    nextPositionInArray.Column = position.PositionInArray.Column;
                    itsPossibleNextMove = true;
                }
            }
            else if (move == Right)
            {
                if (position.PositionInArray.Column + 1 < _dataMap.GetLength(0))
                {
                    nextPositionInArray.Row = position.PositionInArray.Row;
                    nextPositionInArray.Column = position.PositionInArray.Column + 1;
                    itsPossibleNextMove = true;
                }
            }
            else if (move == Left)
            {
                if (position.PositionInArray.Column - 1 >= 0)
                {
                    nextPositionInArray.Row = position.PositionInArray.Row;
                    nextPositionInArray.Column = position.PositionInArray.Column - 1;
                    itsPossibleNextMove = true;
                }
            }

            if (itsPossibleNextMove && _dataMap[nextPositionInArray.Row][nextPositionInArray.Column] < position.ValueOfPosition)
            {
                Position newPosition = new Position()
                {
                    Id = _newId++,
                    IdNextPosition = position.Id,
                    ValueOfPosition = _dataMap[nextPositionInArray.Row][nextPositionInArray.Column],
                    PositionInArray = nextPositionInArray,
                    PositionInRow = _actualRowInArray + 1
                };

                _positions.Add(newPosition);
                return 1;
            }
            return 0;
        }

        private int[] GetRouteSelected(Position position)
        {
            var bestRoute = new List<int>() { position.ValueOfPosition };
            Position nextPositions = null;

            do
            {
                nextPositions = _positions
                    .Where(x => x.Id == position.IdNextPosition).SingleOrDefault();

                if (nextPositions != null)
                    bestRoute.Add(nextPositions.ValueOfPosition);

                position = nextPositions;
            }
            while (nextPositions != null);

            return bestRoute.ToArray();

        }

        private Response GetResults()
        {
            _calculatedRouteLength = _positions.OrderByDescending(x => x.PositionInRow).FirstOrDefault().PositionInRow;

            _positions
                .Where(y => y.PositionInRow == _calculatedRouteLength)
                .OrderByDescending(x => x.Id).ToList()
                .ForEach(positions => _fastestRoute.Add(GetRouteSelected(positions)));

            _routeForHighestDrop = _fastestRoute
                .OrderByDescending(x => x.Max() - x.Min()).FirstOrDefault();

            Array.Reverse(_routeForHighestDrop);

            Response response = new Response()
            {
                HighDrop = _routeForHighestDrop.Max() - _routeForHighestDrop.Min(),
                LenghtOfPositions = _calculatedRouteLength + 1,
                RouteForHighestDrop = string.Join("-", _routeForHighestDrop)
            };

            return response;
        }
        #endregion
    }
}

﻿using Addeco.Library;
using System;
using System.Threading;
using System.Windows.Forms;
using static Addeco.Library.Models;

namespace AddecoForm
{
    public partial class FormMain : Form
    {
        #region Declarations
        private Loader _loader;
        #endregion

        #region Properties
        public bool Loading { get; private set; }
        #endregion

        #region Constructors
        public FormMain()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void BtnLoadMap_Click(object sender, EventArgs e)
        {
            LoadMap();
        }
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Loading)
                e.Cancel = true;
        }
        #endregion

        #region Methods
        private void InitLoader()
        {
            _loader = new Loader();
            _loader.OnProgress += OnProgress;
            _loader.OnSuccess += OnSuccess;
        }
        private void LoadMap()
        {
            try
            {
                var openFile = new OpenFileDialog
                {
                    Multiselect = false,
                    Filter = "File text (*.txt)|*.txt"
                }; var result = openFile.ShowDialog();
                if (result != DialogResult.OK)
                    return;

                txtPathMap.Text = openFile.FileName;

                Loading = true;
                txtResponse.Clear();
                InitLoader();
                UpdateControls();
                _loader.Load(openFile.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void OnProgress()
        {
            if (progressBar.InvokeRequired)
            {
                var loader = new Loader.OnProgressDelegate(OnProgress);
                progressBar.Invoke(loader);
            }
            else
            {
                Loading = true;
                UpdateControls();
            }
        }
        private void OnSuccess(Response response)
        {
            if (txtResponse.InvokeRequired)
            {
                var loader = new Loader.OnSuccessDelegate(OnSuccess);
                txtResponse.Invoke(loader, response);
            }
            else
            {
                Loading = false;
                UpdateControls();
                labelProgress.Text = "Progress: success, route calculated";
                txtResponse.Text = $"Length of calculated path is : {response.LenghtOfPositions}\r\nDrop of calculated path is : {response.HighDrop}\r\nalculated path is : {response.RouteForHighestDrop}";
            }
        }
        private void UpdateControls()
        {
            if (Loading)
            {
                btnLoadMap.Enabled = false;
                progressBar.Style = ProgressBarStyle.Marquee;
                labelProgress.Text = "Progress: calculating route...";
            }
            else
            {
                btnLoadMap.Enabled = true;
                progressBar.Style = ProgressBarStyle.Blocks;
                progressBar.Value = 100;
            }
        }
        #endregion
    }

    public class Loader
    {
        #region Declarations
        public delegate void OnProgressDelegate();
        public event OnProgressDelegate OnProgress;

        public delegate void OnSuccessDelegate(Response response);
        public event OnSuccessDelegate OnSuccess;
        #endregion

        #region Methods
        private void Calculated(string path)
        {
            OnProgress?.Invoke();
            var response = new CalculatedRoute().CalculatedPath(path);
            OnSuccess?.Invoke(response);
        }

        internal void Load(string path)
        {
            var thread = new Thread(() => Calculated(path));
            thread.Start();
        }
        #endregion
    }
}

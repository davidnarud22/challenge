﻿
using Addeco.Library;
using System;
using static Addeco.Library.Models;

namespace Addeco
{
    class Program
    {
        static void Main(string[] args)
        {
            Response response = new CalculatedRoute().CalculatedPath("Maps/map.txt");

            Console.WriteLine($"Length of calculated path is : {response.LenghtOfPositions}");
            Console.WriteLine($"Drop of calculated path is : {response.HighDrop}");
            Console.WriteLine($"Calculated path is : {response.RouteForHighestDrop}");
        }
    }
}
